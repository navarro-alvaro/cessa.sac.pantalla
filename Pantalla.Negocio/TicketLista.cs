﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Negocio
{
    public class TicketLista
    {
        public event MostrarPantallaEventHandler MostrarPantalla;
        public delegate void MostrarPantallaEventHandler();
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        private List<Ticket> listaTickets;
        private Datos.TicketLista ticketListaDatos;
        private string colaPlataforma;
        private string colaOdeco;
		private string colaCajas;

        public TicketLista()
        {
            listaTickets = new List<Ticket>();
            ticketListaDatos = new Datos.TicketLista();
        }

        public Ticket this[int index]
        {
            get
            {
                return listaTickets[index];
            }
            set
            {
                listaTickets[index] = value;
            }
        }

        public List<Ticket> Lista
        {
            get { return listaTickets; }
        }

        public int Total
        {
            get { return listaTickets.Count(); }
        }

        public string ColaPlataforma
        {
            get { return colaPlataforma; }
            set { colaPlataforma = value; }
        }

        public string ColaOdeco
        {
            get { return colaOdeco; }
            set { colaOdeco = value; }
        }

		public string ColaCajas
		{
			get { return colaCajas; }
			set { colaCajas = value; }
		}

        public void Listar()
        {
            ticketListaDatos.Listar();
            listaTickets.Clear();
            for (int i = 0; i < ticketListaDatos.Total; i++)
            {
                listaTickets.Add(new Ticket { 
                    Tipo = ticketListaDatos[i].Tipo, 
                    Numero = ticketListaDatos[i].Numero,
                    Estado = ticketListaDatos[i].Estado,
                    Ubicacion = ticketListaDatos[i].Ubicacion
                });
            }
        }

        public void ListarLlamados()
        {
            ticketListaDatos.ListarLlamados();
            listaTickets.Clear();
            for (int i = 0; i < ticketListaDatos.Total; i++)
            {
                listaTickets.Add(new Ticket
                {
                    Tipo = ticketListaDatos[i].Tipo,
                    Numero = ticketListaDatos[i].Numero,
                    Estado = ticketListaDatos[i].Estado,
                    Ubicacion = ticketListaDatos[i].Ubicacion
                });
            }
        }

        public void TicketsEnCola() 
        {
            ticketListaDatos.TicketsEnCola();
            ColaPlataforma = ticketListaDatos.ColaPlataforma;
            ColaOdeco = ticketListaDatos.ColaOdeco;
			ColaCajas = ticketListaDatos.ColaCajas;
        }
        
    }
}
