﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Negocio
{
    public class MensajeLista
    {
        public event MostrarPantallaEventHandler MostrarPantalla;
        public delegate void MostrarPantallaEventHandler();
        public event ErroresEventHandler Errores;
        public delegate void ErroresEventHandler(string mensaje);

        private List<Mensaje> listaMensajes;

        public MensajeLista()
        {
            listaMensajes = new List<Mensaje>();
        }

        public Mensaje this[int index]
        {
            get
            {
                return listaMensajes[index];
            }
            set
            {
                listaMensajes[index] = value;
            }
        }

        public List<Mensaje> Lista
        {
            get { return listaMensajes; }
        }

        public int Total
        {
            get { return listaMensajes.Count(); }
        }

        public void Listar()
        {
            Datos.MensajeLista mensajeListaDatos = new Datos.MensajeLista();
            mensajeListaDatos.Listar();
            listaMensajes.Clear();
            for (int i = 0; i < mensajeListaDatos.Total; i++)
            {
                listaMensajes.Add(new Mensaje
                {
                    Texto = mensajeListaDatos[i].Texto
                });
            }
        }
    }
}
