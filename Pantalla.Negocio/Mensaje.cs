﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Negocio
{
    public class Mensaje
    {
        //Instancia de Datos para recuperar información
        Datos.Mensaje mensajeDato;

        public Mensaje()
        {
            mensajeDato = new Datos.Mensaje();
        }

        //Propiedades
        public string Texto
        {
            get { return mensajeDato.Texto; }
            set { mensajeDato.Texto = value; }
        }

        //Métodos
    }
}
