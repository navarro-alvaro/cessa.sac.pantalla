﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Negocio
{
    public class Ticket
    {
        //Instancia de Datos para recuperar información
        Datos.Ticket ticketDato;

        public Ticket()
        {
            ticketDato = new Datos.Ticket();
        }

        //Propiedades
        public string Tipo
        {
            get { return ticketDato.Tipo; }
            set { ticketDato.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketDato.Numero; }
            set { ticketDato.Numero = value; }
        }
        public string Estado
        {
            get { return ticketDato.Estado; }
            set { ticketDato.Estado = value; }
        }
        public string Ubicacion
        {
            get { return ticketDato.Ubicacion; }
            set { ticketDato.Ubicacion = value; }
        }

        //Métodos
        
    }
}
