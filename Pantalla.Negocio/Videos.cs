﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Negocio
{
    public class Videos
    {
        Datos.Videos videosDatos = new Datos.Videos();

        public void InsertarVideo(string ruta, string nombre_archivo)
        {
            videosDatos.InsertarVideo(ruta, nombre_archivo);
        }

        public List<string> ObtenerVideos()
        {
            return videosDatos.ObtenerVideos();
        }

        public void ComprobarRuta(string ruta)
        {
            videosDatos.ComprobarRuta(ruta);
        }

        public void ReiniciarEstados()
        {
            videosDatos.ReiniciarEstados();
        }

        public void ComprobarExistencia(string nombre_archivo)
        {
            videosDatos.ComprobarExistencia(nombre_archivo);
        }

        public void EliminarDatos()
        {
            videosDatos.EliminarDatos();
        }

        public int ObtenerCantidad(string nombre_archivo)
        {
            return videosDatos.ObtenerCantidad(nombre_archivo);
        }
    }
}
