﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Datos
{
    public class Videos
    {
        public Videos()
        {
            videoEnUso.RutaArchivo = string.Empty;
            videoEnUso.NombreArchivo = string.Empty;
            videoEnUso.CantidadReproducciones = 0;
        }

        private struct TVideo
        {
            public int Id;
            public string RutaArchivo;
            public string NombreArchivo;
            public int CantidadReproducciones;
        }
        TVideo videoEnUso = new TVideo();

        public int Id
        {
            get { return videoEnUso.Id; }
            set { videoEnUso.Id = value; }
        }

        public string RutaArchivo
        {
            get { return videoEnUso.RutaArchivo; }
            set { videoEnUso.RutaArchivo = value; }
        }

        public string NombreArchivo
        {
            get { return videoEnUso.NombreArchivo; }
            set { videoEnUso.NombreArchivo = value; }
        }

        public int CantidadReproducciones
        {
            get { return videoEnUso.CantidadReproducciones; }
            set { videoEnUso.CantidadReproducciones = value; }
        }

        public bool ComprobarVideo(string ruta, string nombre_archivo)
        {
            try
            {
                Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
                string query = "SELECT * FROM videos WHERE ruta_archivo = '" + ruta.Replace(@"\", @"\\") + "' AND nombre_archivo = '" + nombre_archivo.Replace(@"\", @"\\") + "'";
                MySqlDataReader dtrVideo = dbMySQL.Seleccionar(query);
                if (dtrVideo.Read())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("No puede conectar al servidor. Contacte al Administrador. Videos.cs");
                return false;
            }
        }

        public void InsertarVideo(string ruta, string nombre_archivo)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            if (ComprobarVideo(ruta, nombre_archivo))
            {
                string query = "INSERT INTO videos (ruta_archivo, nombre_archivo, cantidad_reproducciones, estado) VALUES ('" + ruta.Replace(@"\", @"\\") + "', '" + nombre_archivo.Replace(@"\", @"\\") + "', 1, '1')";
                dbMySQL.Insertar(query);
            }
        }

        public List<string> ObtenerVideos()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            List<string> videos = new List<string>();
            try
            {
                string query = "SELECT nombre_archivo FROM videos WHERE cantidad_reproducciones >= 1 and estado = '1'";
                MySqlDataReader dtrVideo = dbMySQL.Seleccionar(query);
                
                while (dtrVideo.Read())
                {
                    videos.Add(dtrVideo.GetString(0));
                }
                return videos;
            }
            catch(Exception ex)
            {
                return videos;
            }

        }

        public void ComprobarRuta(string ruta)
        {
            try
            {
                Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
                string query = "UPDATE videos SET cantidad_reproducciones = 0 WHERE ruta_archivo <> '" + ruta.Replace(@"\", @"\\") + "'";
                dbMySQL.Actualizar(query);
            }
            catch (Exception ex) { }
        }

        public void ReiniciarEstados()
        {
            try
            {
                Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
                string query = "UPDATE videos SET estado = '0'";
                dbMySQL.Actualizar(query);
            }
            catch (Exception ex) { }
        }

        public void ComprobarExistencia(string nombre_archivo)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "UPDATE videos SET estado = '1' WHERE nombre_archivo = '" + nombre_archivo.Replace(@"\", @"\\") + "'";
            dbMySQL.Actualizar(query);
        }

        public void EliminarDatos()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "DELETE FROM videos WHERE estado = '0'";
            dbMySQL.Eliminar(query);
        }

        public int ObtenerCantidad(string nombre_archivo)
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT cantidad_reproducciones FROM videos WHERE nombre_archivo = '" + nombre_archivo.Replace(@"\", @"\\") + "'";
            MySqlDataReader dtrVideo = dbMySQL.Seleccionar(query);

            if (dtrVideo.Read())
            {
                return dtrVideo.GetInt32(0);
            }
            else
            {
                return 0;
            }
        }
    }
}
