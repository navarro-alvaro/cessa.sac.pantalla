﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Datos
{
    public class TicketLista
    {
        private List<Ticket> listaTickets;
        private string colaPlataforma;
        private string colaOdeco;
		private string colaCajas;

        private int contadorCajas = 0;
        private bool hayMayoresCajas = false;
        private string consultaCajas;

        private int contadorPlataforma = 0;
        private bool hayMayoresPlataforma = false;
        private string consultaPlataforma;

        public TicketLista()
        {
            listaTickets = new List<Ticket>();
        }

        // Propiedades
        public Ticket this[int index]
        {
            get
            {
                return listaTickets[index];
            }
            set
            {
                listaTickets[index] = value;
            }
        }

        public List<Ticket> Lista
        {
            get { return listaTickets; }
        }

        public int Total
        {
            get { return listaTickets.Count(); }
        }

        public string ColaPlataforma
        {
            get { return colaPlataforma; }
            set { colaPlataforma = value; }
        }

        public string ColaOdeco
        {
            get { return colaOdeco; }
            set { colaOdeco = value; }
        }

		public string ColaCajas
		{
			get { return colaCajas; }
			set { colaCajas = value; }
		}

        public void Listar()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            try
            {
                string query = "SELECT tipo, numero, estado, ubicacion FROM tickets WHERE fecha_solicitud = CURDATE() AND (estado > 1 AND estado < 6) ORDER BY estado ASC, hora_atencion DESC LIMIT 6";
                MySqlDataReader dtrTickets = dbMySQL.Seleccionar(query);
                listaTickets.Clear();
                while (dtrTickets.Read())
                {
                    listaTickets.Add(new Ticket
                    {
                        Tipo = dtrTickets["tipo"].ToString(),
                        Numero = Convert.ToInt16(dtrTickets["numero"]),
                        Estado = dtrTickets["estado"].ToString(),
                        Ubicacion = dtrTickets["ubicacion"].ToString()
                    });
                }
                dbMySQL.CerrarConexion();
            }
            catch(Exception ex)
            {
                dbMySQL.CerrarConexion();
            }
        }

        public void ListarLlamados()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            try
            {
                string query = "SELECT tipo, numero, estado, ubicacion FROM tickets WHERE fecha_solicitud = CURDATE() AND estado = 2 LIMIT 1";
                MySqlDataReader dtrTickets = dbMySQL.Seleccionar(query);
                listaTickets.Clear();
                while (dtrTickets.Read())
                {
                    listaTickets.Add(new Ticket
                    {
                        Tipo = dtrTickets["tipo"].ToString(),
                        Numero = Convert.ToInt32(dtrTickets["numero"]),
                        Estado = dtrTickets["estado"].ToString(),
                        Ubicacion = dtrTickets["ubicacion"].ToString()
                    });
                }
                dbMySQL.CerrarConexion();
            }
            catch(Exception ex)
            {
                dbMySQL.CerrarConexion();
            }
        }

        public void ListarPorAtender()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT tipo, numero, ubicacion FROM tickets WHERE fecha_solicitud = CURDATE() AND estado = '2' ORDER BY tipo ASC, numero ASC";
            MySqlDataReader dtrTickets = dbMySQL.Seleccionar(query);
            while (dtrTickets.Read())
            {
                listaTickets.Add(new Ticket { Tipo = dtrTickets["tipo"].ToString(), Numero = Convert.ToInt16(dtrTickets["numero"]) });
            }
            dbMySQL.CerrarConexion();
        }

        public void TicketsEnCola()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            try
            {
                string query = "";
                MySqlDataReader dtrTicket;
                /*
                string query = "SELECT tipo, numero FROM tickets WHERE fecha_solicitud = CURDATE() AND estado = '1' AND tipo IN ('A', 'B', 'C') ORDER BY tipo DESC, numero ASC LIMIT 1";
                MySqlDataReader dtrTicket = dbMySQL.Seleccionar(query);
                if (dtrTicket.Read())
                {
                    ColaPlataforma = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0'); 
                }
                else
                {
                    ColaPlataforma = "-----";
                }
                dbMySQL.CerrarConexion();
                */

                query = "SELECT tipo, numero FROM tickets WHERE fecha_solicitud = CURDATE() AND estado < '2'";
                consultaPlataforma = query + " AND tipo = 'A' ORDER BY estado DESC, numero ASC";
                dtrTicket = dbMySQL.Seleccionar(consultaPlataforma);
                if (dtrTicket.Read())
                {
                    if (contadorPlataforma >= 0 && contadorPlataforma < 1)
                    {
                        hayMayoresPlataforma = false;
                        dtrTicket.Close();
                        consultaPlataforma = query + " AND tipo = 'A' ORDER BY estado DESC, numero ASC";
                        dtrTicket = dbMySQL.Seleccionar(consultaPlataforma);
                        if (dtrTicket.Read())
                        {
                            dtrTicket.Close();
                            consultaPlataforma = query + " AND tipo IN ('B', 'C') ORDER BY estado DESC, numero ASC";
                            dtrTicket = dbMySQL.Seleccionar(consultaPlataforma);
                            if (!dtrTicket.Read())
                            {
                                contadorPlataforma = 0;
                            }
                        }
                        dtrTicket.Close();
                    }
                    else
                    {
                        hayMayoresPlataforma = true;
                    }
                }
                else
                {
                    hayMayoresPlataforma = true;
                }
                dtrTicket.Close();
                if (hayMayoresPlataforma)
                {
                    consultaPlataforma = query + " AND tipo IN ('B', 'C') ORDER BY estado DESC, numero ASC";
                    dtrTicket = dbMySQL.Seleccionar(consultaPlataforma);
                    if (dtrTicket.Read())
                    {
                        ColaPlataforma = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0');
                    }
                    else
                    {
                        ColaPlataforma = "-----";
                    }
                    dtrTicket.Close();
                    contadorPlataforma = 0;
                }
                else
                {
                    consultaPlataforma = query + " AND tipo = 'A' ORDER BY estado DESC, numero ASC";
                    dtrTicket = dbMySQL.Seleccionar(consultaPlataforma);
                    if (dtrTicket.Read())
                    {
                        ColaPlataforma = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0');
                    }
                    else
                    {
                        ColaPlataforma = "-----";
                    }
                    dtrTicket.Close();
                    contadorPlataforma++;
                }
                dbMySQL.CerrarConexion();

                query = "SELECT tipo, numero FROM tickets WHERE fecha_solicitud = CURDATE() AND estado = '1' AND tipo IN ('D', 'E', 'F') ORDER BY tipo DESC, numero ASC LIMIT 1";
                dtrTicket = dbMySQL.Seleccionar(query);
                if (dtrTicket.Read())
                {
                    ColaOdeco = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0');
                }
                else
                {
                    ColaOdeco = "-----";
                }
                dbMySQL.CerrarConexion();

                query = "SELECT tipo, numero FROM tickets WHERE fecha_solicitud = CURDATE() AND estado < '2'";
                consultaCajas = query + " AND tipo = 'G' ORDER BY estado DESC, numero ASC";
                dtrTicket = dbMySQL.Seleccionar(consultaCajas);
                if (dtrTicket.Read())
                {
                    if (contadorCajas >= 0 && contadorCajas < 1)
                    {
                        hayMayoresCajas = false;
                        dtrTicket.Close();
                        consultaCajas = query + " AND tipo = 'G' ORDER BY estado DESC, numero ASC";
                        dtrTicket = dbMySQL.Seleccionar(consultaCajas);
                        if (dtrTicket.Read())
                        {
                            dtrTicket.Close();
                            consultaCajas = query + " AND tipo IN ('H', 'I') ORDER BY estado DESC, numero ASC";
                            dtrTicket = dbMySQL.Seleccionar(consultaCajas);
                            if (!dtrTicket.Read())
                            {
                                contadorCajas = 0;
                            }
                        }
                        dtrTicket.Close();
                    }
                    else
                    {
                        hayMayoresCajas = true;
                    }
                }
                else
                {
                    hayMayoresCajas = true;
                }
                dtrTicket.Close();
                if (hayMayoresCajas)
                {
                    consultaCajas = query + " AND tipo IN ('H', 'I') ORDER BY estado DESC, numero ASC";
                    dtrTicket = dbMySQL.Seleccionar(consultaCajas);
                    if (dtrTicket.Read())
                    {
                        ColaCajas = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0');
                    }
                    else
                    {
                        ColaCajas = "-----";
                    }
                    dtrTicket.Close();
                    contadorCajas = 0;
                }
                else
                {
                    consultaCajas = query + " AND tipo = 'G' ORDER BY estado DESC, numero ASC";
                    dtrTicket = dbMySQL.Seleccionar(consultaCajas);
                    if (dtrTicket.Read())
                    {
                        ColaCajas = dtrTicket["tipo"].ToString() + "-" + dtrTicket["numero"].ToString().PadLeft(3, '0');
                    }
                    else
                    {
                        ColaCajas = "-----";
                    }
                    dtrTicket.Close();
                    contadorCajas++;
                }
                dbMySQL.CerrarConexion();
            }
            catch (Exception ex)
            {
                dbMySQL.CerrarConexion();
            }
		}
	}
}
