﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Pantalla.Datos
{
    public class Ticket
    {
        public Ticket()
        {
            ticketEnUso.Tipo = string.Empty;
            ticketEnUso.Numero = 0;
            ticketEnUso.Estado = string.Empty;
            ticketEnUso.Ubicacion = string.Empty;
        }

        //Estructura para almacenar la información
        private struct TTicket
        {
            public string Tipo;
            public int Numero;
            public string Estado;
            public string Ubicacion;
        }
        TTicket ticketEnUso = new TTicket();

        //Propiedades
        public string Tipo
        {
            get { return ticketEnUso.Tipo; }
            set { ticketEnUso.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketEnUso.Numero; }
            set { ticketEnUso.Numero = value; }
        }
        public string Estado
        {
            get { return ticketEnUso.Estado; }
            set { ticketEnUso.Estado = value; }
        }
        public string Ubicacion
        {
            get { return ticketEnUso.Ubicacion; }
            set { ticketEnUso.Ubicacion = value; }
        }

        //Métodos
    }
}
