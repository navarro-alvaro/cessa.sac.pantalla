﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla.Datos
{
    public class MensajeLista
    {
        private List<Mensaje> listaMensajes;

        public MensajeLista()
        {
            listaMensajes = new List<Mensaje>();
        }

        // Propiedades
        public Mensaje this[int index]
        {
            get
            {
                return listaMensajes[index];
            }
            set
            {
                listaMensajes[index] = value;
            }
        }

        public List<Mensaje> Lista
        {
            get { return listaMensajes; }
        }

        public int Total
        {
            get { return listaMensajes.Count(); }
        }

        public void Listar()
        {
            Librerias.DBMySQL dbMySQL = new Librerias.DBMySQL();
            string query = "SELECT texto FROM mensajes WHERE fecha_inicio <= CURDATE() AND fecha_conclusion >= CURDATE() ORDER BY creado DESC";
            MySqlDataReader dtrMensajes = dbMySQL.Seleccionar(query);
            listaMensajes.Clear();
            try
            {
                while (dtrMensajes.Read())
                {
                    listaMensajes.Add(new Mensaje
                    {
                        Texto = dtrMensajes["texto"].ToString()
                    });
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se puede conectar con el servidor. Contacte al Administrador. MensajeLista.cs");
                Application.Exit();
            }
            
            dbMySQL.CerrarConexion();
        }
    }
}