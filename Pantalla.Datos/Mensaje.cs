﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Datos
{
    public class Mensaje
    {
        public Mensaje()
        {
            mensajeEnUso.Texto = string.Empty;
        }

        //Estructura para almacenar la información
        private struct TMensaje
        {
            public string Texto;
        }
        TMensaje mensajeEnUso = new TMensaje();

        //Propiedades
        public string Texto
        {
            get { return mensajeEnUso.Texto; }
            set { mensajeEnUso.Texto = value; }
        }

        //Métodos
    }
}
