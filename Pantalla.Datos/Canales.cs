﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pantalla.Datos
{
    public class Canales
    {
        public int traduccion_canal(int capturado)
        {
            int traducido = 0;
            switch (capturado)
            {
                case 8:
                    traducido = 66;
                    break;
                case 10:
                    traducido = 75;
                    break;
                case 12:
                    traducido = 81;
                    break;
                case 14:
                    traducido = 40;
                    break;
                case 15:
                    traducido = 42;
                    break;
                case 16:
                    traducido = 44;
                    break;
                case 17:
                    traducido = 47;
                    break;
                case 20:
                    traducido = 55;
                    break;
                case 21:
                    traducido = 58;
                    break;
                case 22:
                    traducido = 61;
                    break;
                case 23:
                    traducido = 89;
                    break;
                case 24:
                    traducido = 90;
                    break;
                case 25:
                    traducido = 93;
                    break;
                case 26:
                    traducido = 96;
                    break;
                case 27:
                    traducido = 100;
                    break;
                case 28:
                    traducido = 103;
                    break;
                case 29:
                    traducido = 107;
                    break;
                case 30:
                    traducido = 109;
                    break;
                case 31:
                    traducido = 111;
                    break;
                case 32:
                    traducido = 113;
                    break;
                case 33:
                    traducido = 116;
                    break;
                case 34:
                    traducido = 118;
                    break;
                case 35:
                    traducido = 121;
                    break;
                case 36:
                    traducido = 123;
                    break;
                case 37:
                    traducido = 124;
                    break;
                case 38:
                    traducido = 126;
                    break;
                case 39:
                    traducido = 128;
                    break;
                case 40:
                    traducido = 129;
                    break;
                case 41:
                    traducido = 130;
                    break;
                case 42:
                    traducido = 132;
                    break;
                case 43:
                    traducido = 134;
                    break;
                case 44:
                    traducido = 135;
                    break;
                case 45:
                    traducido = 136;
                    break;
                case 46:
                    traducido = 138;
                    break;
                case 47:
                    traducido = 140;
                    break;
                case 48:
                    traducido = 141;
                    break;
                case 49:
                    traducido = 142;
                    break;
                case 50:
                    traducido = 144;
                    break;
                case 51:
                    traducido = 146;
                    break;
                case 52:
                    traducido = 147;
                    break;
                case 53:
                    traducido = 148;
                    break;
                case 54:
                    traducido = 150;
                    break;
                case 55:
                    traducido = 152;
                    break;
                case 56:
                    traducido = 153;
                    break;
                case 57:
                    traducido = 154;
                    break;
                case 58:
                    traducido = 156;
                    break;
                case 59:
                    traducido = 158;
                    break;
                case 60:
                    traducido = 159;
                    break;
                case 61:
                    traducido = 160;
                    break;
                case 62:
                    traducido = 162;
                    break;
                case 63:
                    traducido = 164;
                    break;
                case 64:
                    traducido = 165;
                    break;
                case 65:
                    traducido = 166;
                    break;
                case 66:
                    traducido = 168;
                    break;
                case 67:
                    traducido = 171;
                    break;
                case 68:
                    traducido = 174;
                    break;
                case 69:
                    traducido = 177;
                    break;
                case 70:
                    traducido = 179;
                    break;
                case 71:
                    traducido = 183;
                    break;
                case 72:
                    traducido = 185;
                    break;
                case 73:
                    traducido = 187;
                    break;
                case 74:
                    traducido = 189;
                    break;
                case 75:
                    traducido = 192;
                    break;
                case 76:
                    traducido = 195;
                    break;
                case 77:
                    traducido = 197;
                    break;
                case 78:
                    traducido = 200;
                    break;
                case 79:
                    traducido = 204;
                    break;
                case 80:
                    traducido = 207;
                    break;
                case 82:
                    traducido = 214;
                    break;
                case 83:
                    traducido = 218;
                    break;
                case 85:
                    traducido = 222;
                    break;
                case 86:
                    traducido = 224;
                    break;
                case 87:
                    traducido = 227;
                    break;
                case 88:
                    traducido = 230;
                    break;
                case 89:
                    traducido = 233;
                    break;
                case 90:
                    traducido = 236;
                    break;
                case 91:
                    traducido = 240;
                    break;
                case 92:
                    traducido = 242;
                    break;
                case 93:
                    traducido = 245;
                    break;
                case 94:
                    traducido = 248;
                    break;
                case 98:
                    traducido = 35;
                    break;
                case 99:
                    traducido = 37;
                    break;
                case 100:
                    traducido = 252;
                    break;
                case 101:
                    traducido = 256;
                    break;
                case 102:
                    traducido = 259;
                    break;
                case 103:
                    traducido = 262;
                    break;
                case 104:
                    traducido = 266;
                    break;
                case 105:
                    traducido = 269;
                    break;
                case 106:
                    traducido = 272;
                    break;
                case 108:
                    traducido = 278;
                    break;
                case 112:
                    traducido = 290;
                    break;
                case 113:
                    traducido = 293;
                    break;
                case 114:
                    traducido = 296;
                    break;
                case 115:
                    traducido = 300;
                    break;
                case 120:
                    traducido = 314;
                    break;
                default:
                    traducido = 44;
                    break;
            }
            return traducido;
        }
    }
}
