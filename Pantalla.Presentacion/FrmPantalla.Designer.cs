﻿namespace Pantalla.Presentacion
{
    partial class FrmPantalla
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPantalla));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.TxtProximosTickets = new System.Windows.Forms.TextBox();
            this.DtgvProximosTickets = new System.Windows.Forms.DataGridView();
            this.dtgvTickets = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.WbrMarquesina = new System.Windows.Forms.WebBrowser();
            this.WmpVideo = new AxWMPLib.AxWindowsMediaPlayer();
            this.TmrTickets = new System.Windows.Forms.Timer(this.components);
            this.TmrLlamados = new System.Windows.Forms.Timer(this.components);
            this.TmrHora = new System.Windows.Forms.Timer(this.components);
            this.lblHora = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvProximosTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTickets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WmpVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 457F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 550F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(892, 560);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.TxtProximosTickets, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.DtgvProximosTickets, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.dtgvTickets, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(451, 544);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // TxtProximosTickets
            // 
            this.TxtProximosTickets.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtProximosTickets.BackColor = System.Drawing.Color.Yellow;
            this.TxtProximosTickets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TxtProximosTickets.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProximosTickets.Location = new System.Drawing.Point(0, 404);
            this.TxtProximosTickets.Margin = new System.Windows.Forms.Padding(0);
            this.TxtProximosTickets.Name = "TxtProximosTickets";
            this.TxtProximosTickets.ReadOnly = true;
            this.TxtProximosTickets.Size = new System.Drawing.Size(451, 33);
            this.TxtProximosTickets.TabIndex = 3;
            this.TxtProximosTickets.TabStop = false;
            this.TxtProximosTickets.Text = "Próximos Tickets";
            this.TxtProximosTickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DtgvProximosTickets
            // 
            this.DtgvProximosTickets.AllowUserToAddRows = false;
            this.DtgvProximosTickets.AllowUserToDeleteRows = false;
            this.DtgvProximosTickets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtgvProximosTickets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DtgvProximosTickets.BackgroundColor = System.Drawing.Color.White;
            this.DtgvProximosTickets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DtgvProximosTickets.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DtgvProximosTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgvProximosTickets.Location = new System.Drawing.Point(3, 444);
            this.DtgvProximosTickets.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.DtgvProximosTickets.MultiSelect = false;
            this.DtgvProximosTickets.Name = "DtgvProximosTickets";
            this.DtgvProximosTickets.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DtgvProximosTickets.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DtgvProximosTickets.RowHeadersVisible = false;
            this.DtgvProximosTickets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            this.DtgvProximosTickets.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.DtgvProximosTickets.RowTemplate.Height = 70;
            this.DtgvProximosTickets.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.DtgvProximosTickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtgvProximosTickets.ShowCellErrors = false;
            this.DtgvProximosTickets.ShowCellToolTips = false;
            this.DtgvProximosTickets.ShowEditingIcon = false;
            this.DtgvProximosTickets.ShowRowErrors = false;
            this.DtgvProximosTickets.Size = new System.Drawing.Size(445, 97);
            this.DtgvProximosTickets.TabIndex = 0;
            this.DtgvProximosTickets.TabStop = false;
            // 
            // dtgvTickets
            // 
            this.dtgvTickets.AllowUserToAddRows = false;
            this.dtgvTickets.AllowUserToDeleteRows = false;
            this.dtgvTickets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgvTickets.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgvTickets.BackgroundColor = System.Drawing.Color.White;
            this.dtgvTickets.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgvTickets.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dtgvTickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvTickets.DefaultCellStyle = dataGridViewCellStyle4;
            this.dtgvTickets.Location = new System.Drawing.Point(3, 83);
            this.dtgvTickets.MultiSelect = false;
            this.dtgvTickets.Name = "dtgvTickets";
            this.dtgvTickets.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dtgvTickets.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dtgvTickets.RowHeadersVisible = false;
            this.dtgvTickets.RowHeadersWidth = 50;
            this.dtgvTickets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtgvTickets.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dtgvTickets.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtgvTickets.RowTemplate.Height = 80;
            this.dtgvTickets.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dtgvTickets.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dtgvTickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgvTickets.ShowCellErrors = false;
            this.dtgvTickets.ShowCellToolTips = false;
            this.dtgvTickets.ShowEditingIcon = false;
            this.dtgvTickets.ShowRowErrors = false;
            this.dtgvTickets.Size = new System.Drawing.Size(445, 318);
            this.dtgvTickets.TabIndex = 0;
            this.dtgvTickets.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Pantalla.Presentacion.Properties.Resources.logo_pantalla;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(445, 74);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.WbrMarquesina, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.WmpVideo, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(465, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(419, 544);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // WbrMarquesina
            // 
            this.WbrMarquesina.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WbrMarquesina.Location = new System.Drawing.Point(3, 477);
            this.WbrMarquesina.MinimumSize = new System.Drawing.Size(20, 20);
            this.WbrMarquesina.Name = "WbrMarquesina";
            this.WbrMarquesina.ScrollBarsEnabled = false;
            this.WbrMarquesina.Size = new System.Drawing.Size(413, 64);
            this.WbrMarquesina.TabIndex = 1;
            // 
            // WmpVideo
            // 
            this.WmpVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WmpVideo.Enabled = true;
            this.WmpVideo.Location = new System.Drawing.Point(3, 3);
            this.WmpVideo.Name = "WmpVideo";
            this.WmpVideo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("WmpVideo.OcxState")));
            this.WmpVideo.Size = new System.Drawing.Size(413, 468);
            this.WmpVideo.TabIndex = 2;
            this.WmpVideo.ClickEvent += new AxWMPLib._WMPOCXEvents_ClickEventHandler(this.WmpVideo_ClickEvent);
            // 
            // TmrTickets
            // 
            this.TmrTickets.Enabled = true;
            this.TmrTickets.Interval = 2000;
            this.TmrTickets.Tick += new System.EventHandler(this.TmrTickets_Tick);
            // 
            // TmrLlamados
            // 
            this.TmrLlamados.Enabled = true;
            this.TmrLlamados.Interval = 2000;
            this.TmrLlamados.Tick += new System.EventHandler(this.TmrLlamados_Tick);
            // 
            // TmrHora
            // 
            this.TmrHora.Enabled = true;
            this.TmrHora.Interval = 1000;
            this.TmrHora.Tick += new System.EventHandler(this.TmrHora_Tick);
            // 
            // lblHora
            // 
            this.lblHora.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHora.AutoSize = true;
            this.lblHora.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(49)))), ((int)(((byte)(114)))));
            this.lblHora.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.White;
            this.lblHora.Location = new System.Drawing.Point(744, 512);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(145, 35);
            this.lblHora.TabIndex = 1;
            this.lblHora.Text = "00:00:00";
            this.lblHora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmPantalla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(916, 584);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmPantalla";
            this.Text = "Sistema de Atención CESSA - Pantalla";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPantalla_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgvProximosTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgvTickets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WmpVideo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.DataGridView dtgvTickets;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer TmrTickets;
        private System.Windows.Forms.DataGridView DtgvProximosTickets;
        private System.Windows.Forms.TextBox TxtProximosTickets;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.WebBrowser WbrMarquesina;
        private AxWMPLib.AxWindowsMediaPlayer WmpVideo;
        private System.Windows.Forms.Timer TmrLlamados;
        private System.Windows.Forms.Timer TmrHora;
        private System.Windows.Forms.Label lblHora;
    }
}

