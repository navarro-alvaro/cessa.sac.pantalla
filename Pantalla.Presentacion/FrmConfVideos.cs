﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla.Presentacion
{
    public partial class FrmConfVideos : Form
    {
        public FrmConfVideos()
        {
            InitializeComponent();
        }

        private void btnCarpeta_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                txtCarpeta.Text = folderDialog.SelectedPath;
            }
        }

        private void btnGuardarCarpeta_Click(object sender, EventArgs e)
        {
            if (!txtCarpeta.Text.Equals(""))
            {
                string rutaVideos = txtCarpeta.Text;

                File.WriteAllText(@"directorioVideos.txt", rutaVideos);

                MessageBox.Show("La carpeta fue seleccionada exitosamente");
                txtCarpeta.ResetText();

                File.WriteAllText(@"Repetir.txt", string.Empty);
            }
            else
            {
                MessageBox.Show("Seleccione la carpeta contenedora antes de continuar", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

        private void btnGuardarYoutube_Click(object sender, EventArgs e)
        {
            if (!(txtYoutube.Text.Equals("")))
            {
                ConfigurationSettings.AppSettings["DirYoutube"] = txtYoutube.Text;

            }
        }

        private void FrmConfVideos_Load(object sender, EventArgs e)
        {
            txtYoutube.Text = ConfigurationSettings.AppSettings["DirYoutube"];
        }
    }
}
