﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using Microsoft.Reporting.WinForms;
using System.Configuration;

namespace Pantalla.Presentacion
{
    public partial class FrmReporte : Form
    {
        public FrmReporte()
        {
            InitializeComponent();
        }

        private void FrmReporte_Load(object sender, EventArgs e)
        {

            string fechaInicio = DateTime.Now.ToString("yyyy-MM-dd");
            string fechaFin = DateTime.Now.ToString("yyyy-MM-dd");
            string sector = "Caja";

            List<ReportParameter> reportParams = new List<ReportParameter>();
            reportParams.Add(new ReportParameter("FechaInicio", fechaInicio));
            reportParams.Add(new ReportParameter("FechaFin", fechaFin));

            reportViewer1.LocalReport.SetParameters(reportParams);

            consultasDataSet ds = new consultasDataSet();
            string ConnectionString = ConfigurationSettings.AppSettings["ConnectionString"];
            MySqlConnection connection = new MySqlConnection();
            connection.ConnectionString = ConnectionString;
            connection.Open();

            string Consulta = "SELECT usuarios.login AS Operador, COUNT(tickets.id) AS TotalAtendidosDia, tickets.fecha_solicitud AS Fecha, tickets.ubicacion as Ubicacion, " +
                "SUBSTRING(SEC_TO_TIME(AVG(TIME_TO_SEC(SUBTIME(tickets.hora_conclusion, tickets.hora_atencion)))), 1, 8) AS TiempoPromedio, " +
                "(SELECT SUBSTRING(SEC_TO_TIME(AVG(TIME_TO_SEC(SUBTIME(tickets.hora_conclusion, tickets.hora_atencion)))), 1, 8) FROM tickets " +
                "WHERE tickets.fecha_solicitud BETWEEN '" + fechaInicio + "' AND '" + fechaFin + "' " +
                "AND tickets.estado = 4 " +
                "AND tickets.ubicacion LIKE ('" + sector + "%') " +
                "AND (SUBTIME(tickets.hora_conclusion, tickets.hora_atencion) > '00:00:30')) AS TiempoPromedioTotal " +
                "FROM tickets " +
                "INNER JOIN usuarios " +
                "ON tickets.usuario_operador_id = usuarios.id " +
                "WHERE tickets.fecha_solicitud BETWEEN '" + fechaInicio + "' AND '" + fechaFin + "' " +
                "AND tickets.estado = 4 " +
                "AND tickets.ubicacion LIKE ('" + sector + "%')" +
                "AND (SUBTIME(tickets.hora_conclusion, tickets.hora_atencion) > '00:00:30') " +
                "GROUP BY tickets.usuario_operador_id";

            MySqlCommand cmd = new MySqlCommand();
            MySqlDataAdapter adapter = new MySqlDataAdapter(Consulta, ConnectionString);
            adapter.Fill(ds, ds.Tables[0].TableName);

            ReportDataSource rds = new ReportDataSource("ComparativaPlataforma", ds.Tables[0]);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(rds);
            reportViewer1.LocalReport.Refresh();

            connection.Close();

            this.reportViewer1.RefreshReport();
        }
    }
}
