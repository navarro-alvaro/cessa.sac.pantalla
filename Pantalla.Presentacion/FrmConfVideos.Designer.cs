﻿namespace Pantalla.Presentacion
{
    partial class FrmConfVideos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCarpeta = new System.Windows.Forms.Button();
            this.txtCarpeta = new System.Windows.Forms.TextBox();
            this.lblCarpeta = new System.Windows.Forms.Label();
            this.btnGuardarCarpeta = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnGuardarYoutube = new System.Windows.Forms.Button();
            this.txtYoutube = new System.Windows.Forms.TextBox();
            this.lblYoutube = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCarpeta);
            this.groupBox1.Controls.Add(this.txtCarpeta);
            this.groupBox1.Controls.Add(this.lblCarpeta);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(419, 73);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Directorio de videos";
            // 
            // btnCarpeta
            // 
            this.btnCarpeta.Location = new System.Drawing.Point(334, 35);
            this.btnCarpeta.Name = "btnCarpeta";
            this.btnCarpeta.Size = new System.Drawing.Size(47, 21);
            this.btnCarpeta.TabIndex = 2;
            this.btnCarpeta.Text = "...";
            this.btnCarpeta.UseVisualStyleBackColor = true;
            this.btnCarpeta.Click += new System.EventHandler(this.btnCarpeta_Click);
            // 
            // txtCarpeta
            // 
            this.txtCarpeta.Location = new System.Drawing.Point(82, 35);
            this.txtCarpeta.Name = "txtCarpeta";
            this.txtCarpeta.ReadOnly = true;
            this.txtCarpeta.Size = new System.Drawing.Size(233, 21);
            this.txtCarpeta.TabIndex = 1;
            // 
            // lblCarpeta
            // 
            this.lblCarpeta.AutoSize = true;
            this.lblCarpeta.Location = new System.Drawing.Point(21, 35);
            this.lblCarpeta.Name = "lblCarpeta";
            this.lblCarpeta.Size = new System.Drawing.Size(37, 18);
            this.lblCarpeta.TabIndex = 0;
            this.lblCarpeta.Text = "Ruta:";
            // 
            // btnGuardarCarpeta
            // 
            this.btnGuardarCarpeta.Location = new System.Drawing.Point(431, 26);
            this.btnGuardarCarpeta.Name = "btnGuardarCarpeta";
            this.btnGuardarCarpeta.Size = new System.Drawing.Size(98, 36);
            this.btnGuardarCarpeta.TabIndex = 3;
            this.btnGuardarCarpeta.Text = "Guardar";
            this.btnGuardarCarpeta.UseVisualStyleBackColor = true;
            this.btnGuardarCarpeta.Click += new System.EventHandler(this.btnGuardarCarpeta_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(552, 132);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.btnGuardarCarpeta);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(544, 101);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Videos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnGuardarYoutube);
            this.tabPage2.Controls.Add(this.txtYoutube);
            this.tabPage2.Controls.Add(this.lblYoutube);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(544, 101);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Youtube";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnGuardarYoutube
            // 
            this.btnGuardarYoutube.Location = new System.Drawing.Point(220, 52);
            this.btnGuardarYoutube.Name = "btnGuardarYoutube";
            this.btnGuardarYoutube.Size = new System.Drawing.Size(84, 32);
            this.btnGuardarYoutube.TabIndex = 2;
            this.btnGuardarYoutube.Text = "Actualizar";
            this.btnGuardarYoutube.UseVisualStyleBackColor = true;
            this.btnGuardarYoutube.Click += new System.EventHandler(this.btnGuardarYoutube_Click);
            // 
            // txtYoutube
            // 
            this.txtYoutube.Location = new System.Drawing.Point(238, 10);
            this.txtYoutube.Name = "txtYoutube";
            this.txtYoutube.Size = new System.Drawing.Size(281, 21);
            this.txtYoutube.TabIndex = 1;
            // 
            // lblYoutube
            // 
            this.lblYoutube.AutoSize = true;
            this.lblYoutube.Location = new System.Drawing.Point(20, 13);
            this.lblYoutube.Name = "lblYoutube";
            this.lblYoutube.Size = new System.Drawing.Size(204, 18);
            this.lblYoutube.TabIndex = 0;
            this.lblYoutube.Text = "Dirección de Youtube por defecto:";
            // 
            // FrmConfVideos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 156);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmConfVideos";
            this.Text = "Configuración";
            this.Load += new System.EventHandler(this.FrmConfVideos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCarpeta;
        private System.Windows.Forms.TextBox txtCarpeta;
        private System.Windows.Forms.Label lblCarpeta;
        private System.Windows.Forms.Button btnGuardarCarpeta;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnGuardarYoutube;
        private System.Windows.Forms.TextBox txtYoutube;
        private System.Windows.Forms.Label lblYoutube;
    }
}