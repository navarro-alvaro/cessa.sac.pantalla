﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Media;
using System.IO;
using System.Configuration;

using CefSharp;
using CefSharp.WinForms;
using DirectX.Capture;
using System.Speech.Synthesis;

namespace Pantalla.Presentacion
{
    public partial class FrmPantalla : Form
    {
        private Negocio.TicketLista listaTicketsNegocio;
        private Negocio.MensajeLista listaMensajesNegocio;

        private Negocio.Canales canalesNegocio;
        private System.Threading.Timer TmrListarTickets;

        private Negocio.TicketLista listaLlamadosNegocio;
        private System.Threading.Timer TmrListarLlamados;

        private Negocio.Videos videosNegocio = new Negocio.Videos();

        private bool pantallaOK = false;
        private bool parpadeoColor = true;
        private bool sonidoTicket = false;
        private bool flag = true;

		// Youtube player with chromium
		ChromiumWebBrowser chromium;

        // Opcion para controlar el cambio de videos;
        int canal;
        string opcion_videos = "";

        // Componentes de captura de TV a traves de tarjeta pci
        Panel panelTV;
        private Capture capture = null;
        private Filters filters;

        // Componentes para mostrar el reporte de atención de usuarios
        Panel panelReporte;

        //Componentes para la conversión de texto en voz
        SpeechSynthesizer synthesizer = new SpeechSynthesizer();
        List<VoiceInfo> vocesInfo = new List<VoiceInfo>();
        bool leer = false;
        bool inicio = true;
        string texto_leer;
        string ticket = "";
        string gestor = "";
        
        private void CargarVideosBD()
        {
            WmpVideo.uiMode = "none";
            WmpVideo.stretchToFit = true;

            string ruta = File.ReadAllText(@"directorioVideos.txt");

            videosNegocio.ComprobarRuta(ruta);

            videosNegocio.ReiniciarEstados();
            
            foreach (var item in Directory.GetFiles(@ruta, "*.mp4"))
            {
                videosNegocio.ComprobarExistencia(item);
                videosNegocio.InsertarVideo(ruta, item);
            }

            videosNegocio.EliminarDatos();

            List<string> videos = videosNegocio.ObtenerVideos();

            foreach (var video in videos)
            {
                int cantidad = videosNegocio.ObtenerCantidad(video);
                for (int i = 0; i < cantidad; i++)
                {
                    WMPLib.IWMPMedia nueva = WmpVideo.newMedia(video);
                    WmpVideo.currentPlaylist.appendItem(nueva);
                }
            }

            WmpVideo.settings.setMode("shuffle", true);
            WmpVideo.settings.setMode("loop", true);
            WmpVideo.Ctlcontrols.next();
            WmpVideo.settings.volume = 25;
            WmpVideo.Ctlcontrols.play();
        }


        /*
         * Método de reproduccion de videos de forma local
         * reemplazado en fecha 08-08-2018
         
        private void CargarVideos()
        {
            WmpVideo.uiMode = "none";
            WmpVideo.stretchToFit = true;

            string ruta = File.ReadAllText(@"directorioVideos.txt");
            string ruta_al_archivo = "";

            string[] archivos = File.ReadAllLines(@"Repetir.txt");
            string ultimaLetra = ruta.Substring(ruta.Length - 1);

            for (int i = 0; i < archivos.Length; i += 2)
            {
                if (ultimaLetra != "\\")
                {
                    ruta_al_archivo = ruta + "\\" + archivos[i];

                }
                else
                {
                    ruta_al_archivo = ruta + archivos[i];
                }

                foreach (var item in Directory.GetFiles(ruta, "*.mp4"))
                {
                    if (item == ruta_al_archivo)
                    {
                        for (int j = 0; j < Convert.ToInt32(archivos[i + 1]); j++)
                        {
                            WMPLib.IWMPMedia nueva = WmpVideo.newMedia(ruta_al_archivo);
                            WmpVideo.currentPlaylist.appendItem(nueva);
                        }
                    }
                }
            }

            foreach (var item in Directory.GetFiles(@ruta, "*.mp4"))
            {
                WMPLib.IWMPMedia nueva = WmpVideo.newMedia(item);
                WmpVideo.currentPlaylist.appendItem(nueva);
            }

            //MessageBox.Show("Videos en reproducción: " + WmpVideo.currentPlaylist.count.ToString());

            //WmpVideo.URL = Application.StartupPath + "\\ListaReproduccion.wpl";
            WmpVideo.settings.setMode("shuffle", true);
            WmpVideo.settings.setMode("loop", true);
            WmpVideo.Ctlcontrols.next();
            WmpVideo.settings.volume = 25;
            WmpVideo.Ctlcontrols.play();
        }*/

        public FrmPantalla(string opcion)
		{
			InitializeComponent();
            opcion_videos = opcion;

            // Clase que se encarga de la traduccion de los canales capturados al formato conocido
            canalesNegocio = new Negocio.Canales();

            //Carga de las voces instaladas en el sistema
            foreach (InstalledVoice voice in synthesizer.GetInstalledVoices())
            {
                vocesInfo.Add(voice.VoiceInfo);
            }

			/** Carga de Mensajes */
			string texto = "";
			listaMensajesNegocio = new Negocio.MensajeLista();

			listaMensajesNegocio.Listar();
			for (int i = 0; i < listaMensajesNegocio.Total; i++)
			{
				if (i > 0) {
					texto += " --- ";
				}
				texto += listaMensajesNegocio[i].Texto.ToString();
			}

			WbrMarquesina.DocumentText = String.Format(
				"<html>" +
				"<body style=\"margin:0;padding:0;font-family:Verdana;\"> " +
				"<p style=\"padding:5px 0;width:100%;background-color:#1e3172;color:#fff;font-size:52px;\"><marquee scrolldelay=\"1\" scrollamount=\"5\" direction=\"left\" loop=\"infinite\">{0}</marquee></p>" +
				"</body></html>", texto);

			switch (opcion)
			{
				case "Carpeta":
                    CargarVideosBD();
					break;
				case "Youtube":
                    // Chromium
                    
                    try
                    {
                        //string dirweb = "https://www.youtube.com";
                        string dirweb = ConfigurationSettings.AppSettings["DirYoutube"];
                        CefSettings settings = new CefSettings();

                        Cef.Initialize(settings);
                        chromium = new ChromiumWebBrowser(dirweb);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

					this.tableLayoutPanel2.Controls.Add(this.chromium, 0, 0);
					chromium.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
					chromium.Enabled = true;
					chromium.Location = new Point(3, 3);
					chromium.Size = new Size(413, 468); //570

					WmpVideo.Visible = false;
					WmpVideo.Ctlcontrols.stop();
					chromium.Visible = true;
                    break;
                case "TV":
                    try
                    {
                        filters = new Filters();
                        capture = new Capture(filters.VideoInputDevices[0], null, true);

                        panelTV = new Panel();
                        this.tableLayoutPanel2.Controls.Add(this.panelTV, 0, 0);
                        panelTV.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
                        panelTV.Enabled = true;
                        panelTV.Location = new Point(3, 3);
                        panelTV.Size = new Size(413, 468);

                        WmpVideo.Visible = false;
                        panelTV.Visible = true;
                        canal = canalesNegocio.traduccion_canal(16);
                        iniciar_TV();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Tarjeta TV no encontrada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                    break;


                    // Reporte en Pantalla
                    /*
                    panelReporte = new Panel();
                    this.tableLayoutPanel2.Controls.Add(this.panelReporte, 0, 0);
                    panelReporte.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
                    panelReporte.Enabled = true;
                    panelReporte.Location = new Point(3, 3);
                    panelReporte.Size = new Size(700, 468);

                    panelReporte.BackColor = Color.Blue;

                    WmpVideo.Visible = false;
                    panelReporte.Visible = true;

                    FrmReporte frmReporte = new FrmReporte();
                    frmReporte.TopLevel = false;
                    //frmReporte.AutoScroll = true;
                    frmReporte.WindowState = FormWindowState.Normal;
                    frmReporte.FormBorderStyle = FormBorderStyle.None;
                    frmReporte.Anchor = ((AnchorStyles)((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
                    frmReporte.Parent = panelReporte;
                    frmReporte.Show();

                    MessageBox.Show("frmReporte: " + "Height=" + frmReporte.Height + " Width=" + frmReporte.Width + "\n" +
                                    "panelReporte: " + "Height=" + panelReporte.Height + " Width=" + panelReporte.Width);
                    
                    break;*/
            }


            /** Obtiene la lista de Tickets */
            listaTicketsNegocio = new Negocio.TicketLista();
            TmrListarTickets = new System.Threading.Timer(TimerPantalla, "ticket", 0, 3000);

            /** Obtiene la lista de los Tickets (estado 2) llamados cada 5 segundos */
            listaLlamadosNegocio = new Negocio.TicketLista();
            TmrListarLlamados = new System.Threading.Timer(TimerLlamados, "ticket", 0, 2000);

            if (inicio)
            {
                if (listaLlamadosNegocio.Total > 0)
                {
                    ticket = listaLlamadosNegocio[0].Tipo.ToString() + "-" + listaLlamadosNegocio[0].Numero.ToString().PadLeft(3, '0');
                    gestor = listaLlamadosNegocio[0].Ubicacion.ToString();
                    switch (gestor)
                    {
                        case "PLT1":
                            gestor = "Plataforma, 1";
                            break;
                        case "PLT2":
                            gestor = "Plataforma, 2";
                            break;
                        case "PLT3":
                            gestor = "Plataforma, 3";
                            break;
                        case "PLT4":
                            gestor = "Plataforma, 4";
                            break;
                        case "PLT5":
                            gestor = "Plataforma, 5";
                            break;
                        case "Caja1":
                            gestor = "Caja, 1";
                            break;
                        case "Caja2":
                            gestor = "Caja, 2";
                            break;
                        case "Caja3":
                            gestor = "Caja, 3";
                            break;
                        case "Caja4":
                            gestor = "Caja, 4";
                            break;
                    }
                    texto_leer = "Tíquet, " + ticket.Substring(0, 1) + ", " + ticket.Substring(2) + ", pase a " + gestor;
                    inicio = false;
                }
            }

            
		}

        private void TmrTickets_Tick(object sender, EventArgs e)
        {
            //UpdateFontSize();
            if (pantallaOK)
            {
                List<Object> listaTickets = new List<Object>();
                for (int i = 0; i < listaTicketsNegocio.Total; i++)
                {
                    listaTickets.Add(new
                    {
                        Ticket = listaTicketsNegocio[i].Tipo.ToString() + "-" + listaTicketsNegocio[i].Numero.ToString().PadLeft(3, '0'),
                        Gestor = listaTicketsNegocio[i].Ubicacion.ToString()
                    });
                }
                dtgvTickets.DataSource = listaTickets;
                dtgvTickets.ClearSelection();
                for (int i = 0; i < listaTicketsNegocio.Total; i++)
                {
                    if (listaTicketsNegocio[i].Estado.ToString() == "2")
                    {
                        if (parpadeoColor)
                        {
                            dtgvTickets.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;
                            dtgvTickets.Rows[i].DefaultCellStyle.ForeColor = Color.Black;
                            dtgvTickets.Rows[i].DefaultCellStyle.Font = new Font("Tahoma", 60F, FontStyle.Bold, GraphicsUnit.Pixel);
                        }
                        else
                        {
                            dtgvTickets.Rows[i].DefaultCellStyle.BackColor = Color.DarkBlue;
                            dtgvTickets.Rows[i].DefaultCellStyle.ForeColor = Color.White;
                            dtgvTickets.Rows[i].DefaultCellStyle.Font = new Font("Tahoma", 60F, FontStyle.Bold, GraphicsUnit.Pixel);
                        }

                        sonidoTicket = true;
                    }
                    else if (listaTicketsNegocio[i].Estado.ToString() == "3")
                    {
                        dtgvTickets.Rows[i].DefaultCellStyle.BackColor = Color.SlateGray;
                        dtgvTickets.Rows[i].DefaultCellStyle.ForeColor = Color.White;
                        dtgvTickets.Rows[i].DefaultCellStyle.Font = new Font("Tahoma", 60F, FontStyle.Bold, GraphicsUnit.Pixel);
                    }
                    else
                    {
                        dtgvTickets.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        dtgvTickets.Rows[i].DefaultCellStyle.ForeColor = Color.Black;
                        dtgvTickets.Rows[i].DefaultCellStyle.Font = new Font("Tahoma", 60F, FontStyle.Bold, GraphicsUnit.Pixel);
                    }
                }

                if (sonidoTicket)
                {
                    SoundPlayer myPlayer = new SoundPlayer();
                    myPlayer.SoundLocation = Application.StartupPath.ToString() + "\\Notify.wav";
                    
                    sonidoTicket = false;

                    int Volumen = 100;
                    int Rate = -2;

                    String nombre = vocesInfo.ElementAt(1).Name;

                    synthesizer.SelectVoice(nombre);

                    synthesizer.Volume = Volumen;
                    synthesizer.Rate = Rate;
                    if (leer)
                    {
                        myPlayer.Play();
                        for (int i = 0; i < 2; i++)
                        {
                            synthesizer.SpeakAsync(texto_leer);
                        }
                        leer = false;
                        
                    }
                }
                
                if (synthesizer.State == SynthesizerState.Speaking)
                {
                    switch (opcion_videos)
                    {
                        case "Carpeta":
                            WmpVideo.settings.volume = 20;
                            break;
                        case "TV":
                            //capture.Tuner.Channel = canalesNegocio.traduccion_canal(canal);
                            break;
                    }
                }
                else
                {
                    switch (opcion_videos)
                    {
                        case "Carpeta":
                            WmpVideo.settings.volume = 100;
                            break;
                        case "TV":
                            //iniciar_TV();
                            break;
                    }
                }
                parpadeoColor = !parpadeoColor;

                /** Listas próximos */
                listaTicketsNegocio.TicketsEnCola();
                List<Object> listaTicketsProximos = new List<Object>();
                listaTicketsProximos.Add(new { 
                    Plataforma = listaTicketsNegocio.ColaPlataforma,
                    //ODECO = listaTicketsNegocio.ColaOdeco
					Cajas = listaTicketsNegocio.ColaCajas
                });

                DtgvProximosTickets.DataSource = listaTicketsProximos;
            }
        }
        
        private void TimerPantalla(object data)
        {
            listaTicketsNegocio.Listar();
            pantallaOK = true;
        }

        private void TimerLlamados(object data)
        {
            listaLlamadosNegocio.ListarLlamados();
        }

		private void pictureBox1_Click_1(object sender, EventArgs e)
		{
            switch (opcion_videos)
            {
                case "Carpeta":
                    WmpVideo.Ctlcontrols.next();
                    break;
                case "TV":
                    string aux = Microsoft.VisualBasic.Interaction.InputBox("Introduzca el número de canal", "Cambiar canal");
                    if (aux != "" && System.Text.RegularExpressions.Regex.IsMatch(aux, @"^[0-9]+$"))
                    {
                        canal = canalesNegocio.traduccion_canal(Convert.ToInt32(aux));
                    }
                    else
                    {
                        canal = canalesNegocio.traduccion_canal(16);
                    }
                    //iniciar_TV();
                    break;
            }
		}

		private void pictureBox1_DoubleClick(object sender, EventArgs e)
		{
            Application.Exit();
		}

		private void FrmPantalla_FormClosing(object sender, FormClosingEventArgs e)
		{
			switch(opcion_videos)
            {
                case "Carpeta":
                    WmpVideo.Ctlcontrols.stop();
                    break;
            }
		}

        private void iniciar_TV()
        {
            capture.Dispose();
            capture = new Capture(filters.VideoInputDevices[0], null, true);
            capture.VideoSource = capture.VideoSources[0];
            capture.Tuner.Channel = canal;
            try
            {
                if (capture.PreviewWindow == null)
                {
                    capture.PreviewWindow = panelTV;
                }
                else
                {
                    capture.PreviewWindow = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al iniciar la tarjeta capturadora de TV. \n\n" + ex.Message + "\n\n" + ex.ToString());
            }
        }

        private void TmrLlamados_Tick(object sender, EventArgs e)
        {
            try
            {
                if (listaLlamadosNegocio.Total > 0)
                {
                    string ticket_nuevo = listaLlamadosNegocio[0].Tipo.ToString() + "-" + listaLlamadosNegocio[0].Numero.ToString().PadLeft(3, '0');
                    string gestor_nuevo = listaLlamadosNegocio[0].Ubicacion.ToString();
                    switch (gestor_nuevo)
                    {
                        case "PLT1":
                            gestor_nuevo = "Plataforma, 1";
                            break;
                        case "PLT2":
                            gestor_nuevo = "Plataforma, 2";
                            break;
                        case "PLT3":
                            gestor_nuevo = "Plataforma, 3";
                            break;
                        case "PLT4":
                            gestor_nuevo = "Plataforma, 4";
                            break;
                        case "PLT5":
                            gestor_nuevo = "Plataforma, 5";
                            break;
                        case "Caja1":
                            gestor_nuevo = "Caja, 1";
                            break;
                        case "Caja2":
                            gestor_nuevo = "Caja, 2";
                            break;
                        case "Caja3":
                            gestor_nuevo = "Caja, 3";
                            break;
                        case "Caja4":
                            gestor_nuevo = "Caja, 4";
                            break;
                    }
                    if (!(ticket_nuevo.Equals(ticket)) || (ticket_nuevo.Equals(ticket) && !(gestor_nuevo.Equals(gestor))))
                    {
                        texto_leer = "Tíquet, " + ticket_nuevo.Substring(0, 1) + ", " + ticket_nuevo.Substring(2) + ", pase a " + gestor_nuevo;
                        ticket = ticket_nuevo;
                        gestor = gestor_nuevo;
                        ticket_nuevo = "";
                        gestor_nuevo = "";
                        leer = true;


                    }
                }
            }
            catch (Exception ex)
            {
                TmrLlamados.Enabled = false;
                //Application.Exit();
            }
        }

        private void WmpVideo_ClickEvent(object sender, AxWMPLib._WMPOCXEvents_ClickEvent e)
        {
            CargarVideosBD();
        }

        public void cambiarColor(bool flag)
        {
            if (flag)
            {
                lblHora.ForeColor = Color.Red;
            }
            else
            {
                lblHora.ForeColor = Color.White;
            }
        }

        private void TmrHora_Tick(object sender, EventArgs e)
        {
            TimeSpan hora = DateTime.Now.TimeOfDay;
            lblHora.Text = hora.ToString().Substring(0, 8);

            TimeSpan hora_inicio_mañana = new TimeSpan(12, 0, 0);
            TimeSpan hora_limite_mañana = new TimeSpan(12, 1, 0);

            TimeSpan hora_inicio_tarde = new TimeSpan(18, 0, 0);
            TimeSpan hora_limite_tarde = new TimeSpan(18, 1, 0);

            if ((hora > hora_inicio_mañana && hora < hora_limite_mañana) || (hora > hora_inicio_tarde && hora < hora_limite_tarde))
            {
                cambiarColor(flag);
                flag = !flag;
            }
        }
    }
}
