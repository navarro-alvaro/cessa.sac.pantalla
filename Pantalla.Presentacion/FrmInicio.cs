﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pantalla.Presentacion
{
	public partial class FrmInicio : Form
	{
		public FrmInicio()
		{
			InitializeComponent();
		}
		FrmPantalla pantalla;

		private void button1_Click(object sender, EventArgs e)
		{
			pantalla = new FrmPantalla("Carpeta");
            this.Hide();
            pantalla.ShowDialog();
            this.Close();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			pantalla = new FrmPantalla("Youtube");
            this.Hide();
            pantalla.ShowDialog();
            this.Close();
		}

        private void button3_Click(object sender, EventArgs e)
        {
            pantalla = new FrmPantalla("TV");
            this.Hide();
            pantalla.ShowDialog();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FrmConfVideos frm = new FrmConfVideos();
            frm.ShowDialog();
        }
    }
}
